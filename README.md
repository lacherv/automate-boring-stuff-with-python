# Automate the Boring Stuff with Python 2nd Edition

## Pratical programming for total Beginners by Al Sweigart

## Part I: Python Programming Basics

### Chapter 1: Python Basics

### Chapter 2: Flow Control

### Chapter 3: Functions

### Chapter 4: Lists

### Chapter 5: Dictionaries and Structuring Data

### Chapter 6: Manipulating String
