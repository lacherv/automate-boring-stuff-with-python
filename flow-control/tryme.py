import random

print('I am thinking of a number between 1 and 20.')
guessN = random.randint(1, 20)
sum = 0
while True:
    print('Take a guess')
    sum = sum + 1
    guess = int(input())
    if guess < guessN:
        print('You guess is too low.')
    if guess > guessN:
        print('Youe guess is too high')
    if guess == guessN:
        break
print('Good job! You guessed my number in ' + str(sum) + ' guesses!')
